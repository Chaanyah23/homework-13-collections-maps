import java.util.*;


public class Homework13 {

    public static void main(String[] args) {

        Map<Integer, String> names = new HashMap<>();
        Map<Integer, String> removeNames = new HashMap<>();

        names.put(1000, "Liam");
        names.put(1001, "Noah");
        names.put(1002, "Olivaia");
        names.put(1003, "Emma");
        names.put(1004, "Benjamin");
        names.put(1005, "Evelyn");
        names.put(1006, "Lucas");

        // Name of person who lives in street number 1004
        System.out.println(names.get(1004));

        for (int i : names.keySet()) {
            if (i % 2 != 0) {
                System.out.println("{key:" + i + " value:" +  names.get(i) + "}");
            }
        }

    }

}
